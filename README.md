# changeBanner

A plugin to [299ko](https://github.com/299Ko/299ko) project. Goaol of this project is make an easy way to change the banner picture on your site. 

## Install
* Copy changeBanner directory in plugin directory
* In admin gui of your 299ko instance, active the plugin

## Howto
* Select the new image on computer
* Click on "Change the banner" button
