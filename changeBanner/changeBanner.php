<?php

/**
 * @copyright (C) 2023, 299Ko
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPLv3
 * @author Quentin Laize <contact@laize.org>
 */
defined('ROOT') OR exit('No direct script access allowed');

##
## Install Function
##
function changeBannerInstall() {
	@mkdir(DATA_PLUGIN . 'changeBanner/uploads');
	@chmod(DATA_PLUGIN . 'changeBanner/uploads', 0755);
}

?>
