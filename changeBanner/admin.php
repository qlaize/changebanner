<?php
/**
 * @copyright (C) 2023, 299Ko
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPLv3
 * @author Quentin Laize <contact@laize.org>
 */
 
defined('ROOT') OR exit('No direct script access allowed');

if( ! is_dir(DATA_PLUGIN . 'changeBanner/uploads') ){
	@mkdir(DATA_PLUGIN . 'changeBanner/uploads');
        @chmod(DATA_PLUGIN . 'changeBanner/uploads', 0755);
}

$dir = $_POST['dir'] ?? $_GET['dir'] ?? '';
$dir = rawurldecode($dir);

if ($dir === 'Back%To%Home%') {
    $dir = '';
}
if ($dir !== '') {

    $dirParts = explode('/', $dir);
    if (end($dirParts) === '..') {
        // Up to parent folder
        array_pop($dirParts);
        array_pop($dirParts);
    }

    $dir = implode('/', $dirParts);
} else {
    $dirParts = [];
}
$fullDir = UPLOAD . 'files/' . trim($dir, '/');

?>
