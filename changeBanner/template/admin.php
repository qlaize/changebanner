<?php
/**
 * @copyright (C) 2023, 299Ko
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPLv3
 * @author Quentin Laize <contact@laize.org>
 */
defined('ROOT') OR exit('No direct script access allowed');

include_once(ROOT . 'admin/header.php');
include_once(PLUGINS . 'changeBanner/template/adminView.php');
include_once(ROOT . 'admin/footer.php');

?>
