<?php
/**
 * @copyright (C) 2023, 299Ko
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GPLv3
 * @author Quentin Laize <contact@laize.org>
 */
defined('ROOT') OR exit('No direct script access allowed');
?>
<section>
	<p>
        	<h3>Thème actuel</h3><br>
		<label>Nom: <?php print($core->getConfigVal('theme')); ?></label><br>
		<label>Fichier css: <?php $core = core::getInstance(); $css = 'theme/' . $core->getConfigVal('theme') . '/styles.css'; print($css); ?></label><br>
		<label>Image de bannière: <?php $file=file_get_contents($css); preg_match("/background: url\('(.*)'\)/",$file,$image); $image = $image[1]; print($image) ?></label><br>
		<h3>Nouvelle image</h3><br>
        	<label id="chageBanner-label" for="bannerImage">Selctionner la bannière</label>
        	<input type="file" name="banniere" id="bannerImage" onchange="setBanner(this)">
        	<input id='bannerInput' type='hidden' value='<?php echo rawurlencode($dir); ?>'>
        	<button id="uploadFile" type="submit" onclick="uploadBanner()" disabled="disabled">Remplacer la bannière</button>
		<?php
            		if ($dir !== '') {
                ?>
                    <div  onClick='refreshView("<?php echo rawurlencode($dir . '/..'); ?>")'>
                    </div>
                <?php
	    		}
		?>
	</p>
</section>
	<script>
		function setBanner(data) {
			let fileName = data.value.split("\\").pop();
			document.getElementById('uploadFile').disabled=false;
        		document.getElementById("chageBanner-label").innerText = fileName;
    		}

		function uploadBanner() {
        		const image_files = document.getElementById('bannerImage').files;
        		if (image_files.length) {
            			document.getElementById("uploadFile").innerHTML = '<i class="fa-solid fa-circle-notch fa-spin fa-2x"></i>';
				document.getElementById("uploadFile").setAttribute('disabled', true);
            			let formData = new FormData();
				formData.append('image', image_files[0]);
				alert(URL.createObjectURL(image_files[0]));
            			formData.append('dir', document.getElementById("bannerInput").value);
				let xhr = new XMLHttpRequest();
            			xhr.open("POST", '<?php echo $uploadUrl; ?>', true);
            			xhr.onreadystatechange = function () {
                			if (xhr.readyState === 4 && xhr.status === 200) {
                    				const data = JSON.parse(this.responseText);
                    				if (data.success === 0) {
                        				Toastify({
                            					text: "Echec de l'envoi du fichier",
                            					className: "error"
                        				}).showToast();
                    				} else {
                        				refreshView("<?php echo rawurlencode($dir); ?>");
                    				}
                			}
            			};
				xhr.send(formData);
        		} else {
            			Toastify({
                			text: "Aucun fichier sélectionné",
                			className: "error"
            			}).showToast();
			}
    		}
	</script>
